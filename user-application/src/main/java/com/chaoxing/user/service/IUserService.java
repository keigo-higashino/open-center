package com.chaoxing.user.service;


import com.chaoxing.user.beans.DTO.Result;
import com.chaoxing.user.beans.VO.RegisterVO;

public interface IUserService {
    Result register(RegisterVO registerVO);
}
