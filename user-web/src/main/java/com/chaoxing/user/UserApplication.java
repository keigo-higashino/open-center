package com.chaoxing.user;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.stereotype.Component;
import tk.mybatis.spring.annotation.MapperScan;

/**
 * @author dingyuanyuan
 */
@SpringBootApplication
@MapperScan("com.chaoxing.user.dao.mapper")
@ComponentScan(basePackages = {"com.chaoxing"})
public class UserApplication {

    public static void main(String[] args) {
        SpringApplication.run(UserApplication.class, args);
    }

}
