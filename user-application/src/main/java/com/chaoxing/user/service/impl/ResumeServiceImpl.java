package com.chaoxing.user.service.impl;

import com.chaoxing.user.beans.DO.Resume;
import org.springframework.beans.factory.annotation.Autowired;

import com.chaoxing.user.beans.DO.UserInfo;
import com.chaoxing.user.beans.DTO.Result;
import com.chaoxing.user.convert.UserInfoConvert;
import com.chaoxing.user.dao.IResumeDao;
import com.chaoxing.user.service.IResumeService;
import org.springframework.stereotype.Service;

import java.util.List;
@Service
public class ResumeServiceImpl implements IResumeService {
    @Autowired
    private IResumeDao resumeDao ;


    @Override
    public Result selectByUserId(Integer id) {

//        UserInfo userInfo =  UserInfoConvert.INSTANCE.userVotoUserInfo(registerVO) ;
        List<Resume> resumes = resumeDao.selectByUserId(id);

        return !resumes.isEmpty() ? Result.custom(1,resumes,"查询成功"):Result.fail("查询失败");

    }
}
