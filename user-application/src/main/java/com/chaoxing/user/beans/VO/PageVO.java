package com.chaoxing.user.beans.VO;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
@ApiModel
public class PageVO {

    @ApiModelProperty(value = "页面数量")
    private Integer pageSize;

    @ApiModelProperty(value = "页码")
    private Integer pageNum;




}
