package com.chaoxing.user.dao.mapper;

import tk.mybatis.mapper.common.Mapper;

import com.chaoxing.user.beans.DO.Resume;
import java.util.List;

public interface ResumeInfoMapper extends Mapper<Resume> {
    public List<Resume> selectByUserId(Integer id);
}
