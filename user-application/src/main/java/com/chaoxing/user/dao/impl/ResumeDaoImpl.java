package com.chaoxing.user.dao.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;


import com.chaoxing.user.beans.DO.Resume;
import com.chaoxing.user.dao.IResumeDao;
import com.chaoxing.user.dao.mapper.ResumeInfoMapper;

import java.util.List;


@Repository
public class ResumeDaoImpl implements IResumeDao {

    @Autowired
    private ResumeInfoMapper resumeInfoMapper ;

    @Override
    public List<Resume> selectByUserId(Integer id) {
        List<Resume> resumes = resumeInfoMapper.selectByUserId(id);
        return resumes;
    }
}