package com.chaoxing.user.dao;

import com.chaoxing.user.beans.DO.Resume;
import java.util.List;

public interface IResumeDao {
    public List<Resume> selectByUserId(Integer id);
}
