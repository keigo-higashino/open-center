package com.chaoxing.user.beans.DO;

import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.Table;
import java.util.Date;

@Table(name = "t_resume")
public class Resume {
    /**
     * 简历id
     */
    @Id
    private Long id;

    /**
     * 用户id
     */
    @Column(name = "user_id")
    private Long userId;

    /**
     * 简历名称
     */
    private String name;

    /**
     * 邮箱
     */
    private String email;

    /**
     * 学历
     */
    private String education;

    /**
     * 工作经历
     */
    private String experience;

    /**
     * 毕业时间
     */
    @Column(name = "graduation_time")
    private Date graduationTime;

    /**
     * 创建时间
     */
    @Column(name = "create_time")
    private Date createTime;

    /**
     * 获取简历id
     *
     * @return uid - 简历id
     */
    public Long getUid() {
        return id;
    }

    /**
     * 设置简历id
     *
     * @param uid 简历id
     */
    public void setUid(Long uid) {
        this.id = uid;
    }

    /**
     * 获取用户id
     *
     * @return user_id - 用户id
     */
    public Long getUserId() {
        return userId;
    }

    /**
     * 设置用户id
     *
     * @param userId 用户id
     */
    public void setUserId(Long userId) {
        this.userId = userId;
    }

    /**
     * 获取简历名称
     *
     * @return name - 简历名称
     */
    public String getName() {
        return name;
    }

    /**
     * 设置简历名称
     *
     * @param name 简历名称
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * 获取邮箱
     *
     * @return email - 邮箱
     */
    public String getEmail() {
        return email;
    }

    /**
     * 设置邮箱
     *
     * @param email 邮箱
     */
    public void setEmail(String email) {
        this.email = email;
    }

    /**
     * 获取学历
     *
     * @return education - 学历
     */
    public String getEducation() {
        return education;
    }

    /**
     * 设置学历
     *
     * @param education 学历
     */
    public void setEducation(String education) {
        this.education = education;
    }

    /**
     * 获取工作经历
     *
     * @return experience - 工作经历
     */
    public String getExperience() {
        return experience;
    }

    /**
     * 设置工作经历
     *
     * @param experience 工作经历
     */
    public void setExperience(String experience) {
        this.experience = experience;
    }

    /**
     * 获取毕业时间
     *
     * @return graduation_time - 毕业时间
     */
    public Date getGraduationTime() {
        return graduationTime;
    }

    /**
     * 设置毕业时间
     *
     * @param graduationTime 毕业时间
     */
    public void setGraduationTime(Date graduationTime) {
        this.graduationTime = graduationTime;
    }

    /**
     * 获取创建时间
     *
     * @return create_time - 创建时间
     */
    public Date getCreateTime() {
        return createTime;
    }

    /**
     * 设置创建时间
     *
     * @param createTime 创建时间
     */
    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }
}