package com.chaoxing.user.convert;

import com.chaoxing.user.beans.DO.UserInfo;
import com.chaoxing.user.beans.VO.RegisterVO;
import javax.annotation.Generated;

@Generated(
    value = "org.mapstruct.ap.MappingProcessor",
    date = "2021-05-14T16:32:03+0800",
    comments = "version: 1.2.0.Final, compiler: javac, environment: Java 1.8.0_131 (Oracle Corporation)"
)
public class UserInfoConvertImpl implements UserInfoConvert {

    @Override
    public UserInfo userVotoUserInfo(RegisterVO registerVO) {
        if ( registerVO == null ) {
            return null;
        }

        UserInfo userInfo = new UserInfo();

        userInfo.setUserPassword( registerVO.getPassword() );
        userInfo.setUserName( registerVO.getName() );
        userInfo.setUserAccount( registerVO.getAccount() );

        return userInfo;
    }
}
