package com.chaoxing.user.controller;



import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.chaoxing.user.beans.DTO.Result;
import com.chaoxing.user.service.IResumeService;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;

@RestController
@Api(tags = "简历接口模块")
public class ResumeController {

    @Resource
    private IResumeService resumeService ;

    @GetMapping("/selectbyuserid")
    @ApiOperation("根据用户id查询")
    @ResponseBody
    public Result selectresume(Integer id) {

        return resumeService.selectByUserId(id);
    }


}
