package com.chaoxing.user.service;

import com.chaoxing.user.beans.DTO.Result;

public interface IResumeService {
    Result selectByUserId(Integer id);
}
