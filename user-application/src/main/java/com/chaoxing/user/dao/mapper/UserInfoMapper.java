package com.chaoxing.user.dao.mapper;


import tk.mybatis.mapper.common.Mapper;

import com.chaoxing.user.beans.DO.UserInfo;

public interface UserInfoMapper extends Mapper<UserInfo> {
}