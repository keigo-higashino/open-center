package com.chaoxing.user.service.impl;

import cn.hutool.crypto.SecureUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.chaoxing.user.beans.DO.UserInfo;
import com.chaoxing.user.beans.DTO.Result;
import com.chaoxing.user.beans.VO.RegisterVO;
import com.chaoxing.user.convert.UserInfoConvert;
import com.chaoxing.user.dao.IUserDao;
import com.chaoxing.user.service.IUserService;

/**
 * @author dingyuanyuan
 *
 *
 */
@Service
public abstract class UserServiceImpl implements IUserService {

    @Autowired
    private IUserDao userDao ;

    @Override
    public Result register(RegisterVO registerVO) {
        UserInfo userInfo =  UserInfoConvert.INSTANCE.userVotoUserInfo(registerVO) ;
        userInfo.setUserPassword(SecureUtil.md5(userInfo.getUserPassword()).toLowerCase());
        return userDao.inserUserInfo(userInfo)>0 ? Result.success("注册成功"):Result.fail("新增用户失败");
    }
}
